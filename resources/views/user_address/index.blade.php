@extends('layouts.layout')

@section('content')
<div class="row">
    <div class="col-lg-12 mb-3 mt-3">
        <h4>CRUD User Address</h4>
        <a class="btn btn-success" href="{{ route('user_addresses.create') }}">Create New User Address</a>
    </div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success mb-3">
    <p>{{ $message }}</p>
</div>
@endif

<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>User Name</th>
        <th>Detail</th>
        <th width="280px">Action</th>
    </tr>
    @foreach ($user_addresses as $user_address)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $user_address->user->name }}</td>
        <td>{{ $user_address->detail }}</td>
        <td>
            <form action="{{ route('user_addresses.destroy',$user_address->id) }}" method="POST">

                <a class="btn btn-secondary" href="{{ route('user_addresses.show',$user_address->id) }}">Show</a>

                <a class="btn btn-primary" href="{{ route('user_addresses.edit',$user_address->id) }}">Edit</a>

                @csrf
                @method('DELETE')

                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
        </td>
    </tr>
    @endforeach
</table>

{!! $user_addresses->links() !!}

@endsection