@extends('layouts.layout')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Show User Address</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('user_addresses.index') }}"> Back</a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>User Name:</strong>
            {{ $user_address->user->name }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Detail Address:</strong>
            {{ $user_address->detail }}
        </div>
    </div>
</div>
@endsection