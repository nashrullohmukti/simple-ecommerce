@extends('layouts.layout')

@section('content')
<div class="row">
    <div class="col-lg-12 mb-3 mt-3">
        <h4>CRUD User</h4>
        <a class="btn btn-success" href="{{ route('users.create') }}"> Create New user</a>
    </div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success mb-3">
    <p>{{ $message }}</p>
</div>
@endif

<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Name</th>
        <th>Email</th>
        <th>Username</th>
        <th width="280px">Action</th>
    </tr>
    @foreach ($users as $user)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $user->name }}</td>
        <td>{{ $user->email }}</td>
        <td>{{ $user->username }}</td>
        <td>
            <form action="{{ route('users.destroy',$user->id) }}" method="POST">

                <a class="btn btn-secondary" href="{{ route('users.show',$user->id) }}">Show</a>

                <a class="btn btn-primary" href="{{ route('users.edit',$user->id) }}">Edit</a>

                @csrf
                @method('DELETE')

                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
        </td>
    </tr>
    @endforeach
</table>

{!! $users->links() !!}

@endsection