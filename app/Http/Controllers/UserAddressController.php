<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserAddress;
use Illuminate\Http\Request;

class UserAddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_addresses = UserAddress::latest()->paginate(5);

        return view('user_address.index', compact('user_addresses'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        return view('user_address.create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_id' => 'required',
            'detail' => 'required',
        ]);

        UserAddress::create($request->all());

        return redirect()->route('user_addresses.index')
            ->with('success', 'User Address created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserAddress  $userAddress
     * @return \Illuminate\Http\Response
     */
    public function show(UserAddress $user_address)
    {
        return view('user_address.show', compact('user_address'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UserAddress  $userAddress
     * @return \Illuminate\Http\Response
     */
    public function edit(UserAddress $user_address)
    {
        $users = User::all();
        return view('user_address.edit', compact('user_address', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UserAddress  $userAddress
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserAddress $user_address)
    {
        $request->validate([
            'user_id' => 'required',
            'detail' => 'required',
        ]);

        $user_address->update($request->all());

        return redirect()->route('user_addresses.index')
            ->with('success', 'User Address updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserAddress  $userAddress
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserAddress $user_address)
    {
        $user_address->delete();

        return redirect()->route('user_addresses.index')
            ->with('success', 'User Address deleted successfully');
    }
}
